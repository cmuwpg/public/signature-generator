import * as React from 'react';
import * as ReactDom from 'react-dom';
import { Version } from '@microsoft/sp-core-library';
import {
  IPropertyPaneConfiguration,
  PropertyPaneTextField
} from '@microsoft/sp-property-pane';
import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';
import { IReadonlyTheme } from '@microsoft/sp-component-base';

import * as strings from 'SignatureGeneratorWebPartStrings';
import SignatureGenerator from './components/SignatureGenerator';
import { ISignatureGeneratorProps } from './components/ISignatureGeneratorProps';
import { spfi, SPFI, SPFx as spSPFx } from "@pnp/sp";
import { graphfi, GraphFI, SPFx as graphSPFx} from "@pnp/graph";

export interface ISignatureGeneratorWebPartProps {
  tollFreeNumber: string;
  websiteText: string;
  websiteUrl: string;
  msWebsiteText: string;
  msWebsiteUrl: string;
  msAddress: string;
  mainNumber: string;
  scentFree: string;
  treatyStatement: string;
  metisStatement: string;
  imagePrefix: string;
}

export default class SignatureGeneratorWebPart extends BaseClientSideWebPart<ISignatureGeneratorWebPartProps> {

  private _isDarkTheme: boolean = false;
  private sp: SPFI = null;
  private graph: GraphFI = null; 

  public render(): void {
    const element: React.ReactElement<ISignatureGeneratorProps> = React.createElement(
      SignatureGenerator,
      {
        tollFreeNumber: this.properties.tollFreeNumber,
        mainNumber: this.properties.mainNumber,
        scentFree: this.properties.scentFree,
        treatyStatement: this.properties.treatyStatement,
        metisStatement: this.properties.metisStatement,
        websiteText: this.properties.websiteText,
        websiteUrl: this.properties.websiteUrl,
        msWebsiteText: this.properties.msWebsiteText,
        msWebsiteUrl: this.properties.msWebsiteUrl,
        imagePrefix: this.properties.imagePrefix,
        msAddress: this.properties.msAddress,
        isDarkTheme: this._isDarkTheme,
        hasTeamsContext: !!this.context.sdks.microsoftTeams,
        userDisplayName: this.context.pageContext.user.displayName,
        sp: this.sp,
        graph: this.graph
      }
    );

    ReactDom.render(element, this.domElement);
  }

  protected async onInit(): Promise<void> {
    const response = await super.onInit();
    this.sp = spfi().using(spSPFx(this.context));
    this.graph = graphfi().using(graphSPFx(this.context));
    return response;
 }

  protected onThemeChanged(currentTheme: IReadonlyTheme | undefined): void {
    if (!currentTheme) {
      return;
    }

    this._isDarkTheme = !!currentTheme.isInverted;
    const {
      semanticColors
    } = currentTheme;

    if (semanticColors) {
      this.domElement.style.setProperty('--bodyText', semanticColors.bodyText || null);
      this.domElement.style.setProperty('--link', semanticColors.link || null);
      this.domElement.style.setProperty('--linkHovered', semanticColors.linkHovered || null);
    }
  }

  protected onDispose(): void {
    ReactDom.unmountComponentAtNode(this.domElement);
  }

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('tollFreeNumber', {
                  label: strings.TollFreeNumberFieldLabel
                }),
                PropertyPaneTextField('mainNumber', {
                  label: strings.MainPhoneNumberFieldLabel
                }),
                PropertyPaneTextField('websiteText', {
                  label: strings.WebsiteTextFieldLabel
                }),
                PropertyPaneTextField('websiteUrl', {
                  label: strings.WebsiteUrlFieldLabel
                }),
                PropertyPaneTextField('imagePrefix', {
                  label: strings.ImagePrefixFieldLabel
                }),
                PropertyPaneTextField('msWebsiteText', {
                  label: strings.MsWebsiteTextFieldLabel
                }),
                PropertyPaneTextField('msWebsiteUrl', {
                  label: strings.MsWebsiteUrlFieldLabel
                }),
                PropertyPaneTextField('msAddress', {
                  label: strings.MsWebsiteUrlFieldLabel
                }),
                PropertyPaneTextField('scentFree', {
                  label: strings.ScentFreeFieldLabel,
                  multiline: true
                }),
                PropertyPaneTextField('treatyStatement', {
                  label: strings.TreatyStatementFieldLabel,
                  multiline: true
                }),
                PropertyPaneTextField('metisStatement', {
                  label: strings.MetisStatementFieldLabel,
                  multiline: true
                })
              ]
            }
          ]
        }
      ]
    };
  }
}