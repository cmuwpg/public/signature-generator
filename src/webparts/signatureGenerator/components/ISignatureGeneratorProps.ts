import { SPFI } from "@pnp/sp";
import { GraphFI } from "@pnp/graph";

export interface ISignatureGeneratorProps {
  tollFreeNumber: string;
  mainNumber: string;
  scentFree: string;
  isDarkTheme: boolean;
  hasTeamsContext: boolean;
  userDisplayName: string;
  websiteText: string;
  websiteUrl: string;
  imagePrefix: string;
  msWebsiteText: string;
  msWebsiteUrl: string;
  msAddress: string;
  treatyStatement: string;
  metisStatement: string;
  sp: SPFI;
  graph: GraphFI;
}

export interface IGraphData {
  userDisplayName: string;
  jobTitle: string;
  mobilePhone: string;
  businessPhones: string[];
  email: string;
}

export type IGraphState =
  { kind: 'GraphFetching', data?: IGraphData } |
  { kind: 'GraphData', data: IGraphData } |
  { kind: 'GraphError' }

export interface IPreferences {
  name: string,
  title: string,
  directPhone: string,
  extension: string,
  useTollFree: boolean,
  useScentFree: boolean,
  useSocialMedia: boolean,
  useTreatyStatement: boolean,
  mennoSimons: boolean,
  pronouns: string 
}

export const SPFields = {
  name: "Title",
  title: "Job_x0020_Title",
  directPhone: "Direct_x0020_Phone",
  extension: "Extension",
  useTollFree: "Use_x0020_Toll_x0020_Free",
  useScentFree: "Use_x0020_Scent_x0020_Free",
  useSocialMedia: "UseSocialMedia",
  useTreatyStatement: "Use_x0020_Treaty_x0020_Statement",
  mennoSimons: "Menno_x0020_Simons",
  pronouns: "Pronouns",
  person: "Person",
  email: "Person/EMail",
  personId: "PersonId",
  id: "Id"
}

export interface SPPreferences {
  Title: string,
  Job_x0020_Title: string,
  Direct_x0020_Phone: string,
  Extension: string,
  UseSocialMedia: boolean,
  Use_x0020_Toll_x0020_Free: boolean,
  Use_x0020_Scent_x0020_Free: boolean,
  Use_x0020_Treaty_x0020_Statement: boolean,
  Menno_x0020_Simons: boolean,
  Pronouns: string
}

export type Preferences =
  { kind: 'Fetching', fetched?: IPreferences, current?: IPreferences } |
  { kind: 'Fetched', fetched: IPreferences, current: IPreferences, id: number } |
  { kind: 'New', current: IPreferences, email: string } |
  { kind: 'Error', error: string, fetched?: IPreferences, current?: IPreferences }

export interface ISignatureGeneratorState {
  prefs: Preferences;
  graphState: IGraphState;
}