import * as React from 'react';
import styles from './SignatureGenerator.module.scss';
import 'office-ui-fabric-react/dist/css/fabric.css';
import { SPFields, IPreferences, ISignatureGeneratorProps, ISignatureGeneratorState, IGraphData, SPPreferences } from './ISignatureGeneratorProps';
import { sanitize } from 'isomorphic-dompurify';
import { encode } from 'html-entities';
import { PrimaryButton, TextField, Toggle, Spinner, SpinnerSize } from 'office-ui-fabric-react';
import '@pnp/graph/users';
import '@pnp/sp/lists';
import '@pnp/sp/webs';
import '@pnp/sp/items';
import "@pnp/sp/site-users";

// Workaround for failure of the usual import in 'dev' mode
import * as ReactTypes from 'react-dom/server'; // eslint-disable-line @typescript-eslint/no-unused-vars
const ReactDOMServer = require('react-dom/cjs/react-dom-server.browser.production.min.js'); // eslint-disable-line @typescript-eslint/no-var-requires

function escape (text: string): {__html: string} {
  return {__html: encode(sanitize(text), {mode: 'nonAsciiPrintable'})};
}

export default class SignatureGenerator extends React.Component<ISignatureGeneratorProps, ISignatureGeneratorState> {
  public constructor(props: ISignatureGeneratorProps) {
    super(props);

    this.state = {
      prefs: {kind: "Fetching"},
      graphState: {kind: "GraphFetching"}
    }

    this.fetchMe().then((data) => this.fetchState(data)).catch((err) => console.log(err));
  }

  private listGUID: string = '92B920C2-CC3C-4171-ADC8-0C30E862F55C';

  public fetchState (graph: IGraphData): Promise<void> {
    switch(this.state.prefs.kind) {
      case 'Fetched':
        this.setState({prefs: {kind: 'Fetching', current: this.state.prefs.current}})
        break;

      case 'Fetching':
        this.setState({prefs: {kind: 'Fetching', current: this.state.prefs.current}})
        break;

      case 'Error':
        this.setState({prefs: {kind: 'Fetching'}})
        break;
    }

    const selection = [
      SPFields.email,
      SPFields.name,
      SPFields.directPhone,
      SPFields.extension,
      SPFields.mennoSimons,
      SPFields.pronouns,
      SPFields.title,
      SPFields.useSocialMedia,
      SPFields.useScentFree,
      SPFields.useTollFree,
      SPFields.useTreatyStatement,
      SPFields.personId,
      SPFields.id
    ];

    return this.props.sp.web.lists.getById(this.listGUID).items.select(...selection)
        .filter("Person/EMail eq '" + graph.email + "'").expand(SPFields.person).getPaged().then((result) => {
      if (result.results.length > 0) {
        const val = result.results[0];
        const converted: IPreferences = {
          directPhone: val[SPFields.directPhone] || '',
          extension: val[SPFields.extension] || '',
          mennoSimons: val[SPFields.mennoSimons],
          name: val[SPFields.name] || '',
          pronouns: val[SPFields.pronouns] || '',
          title: val[SPFields.title] || '',
          useSocialMedia: val[SPFields.useSocialMedia],
          useScentFree: val[SPFields.useScentFree],
          useTollFree: val[SPFields.useTollFree],
          useTreatyStatement: val[SPFields.useTreatyStatement]
        };

        this.setState({
          prefs: {
            kind: 'Fetched',
            id: val[SPFields.id],
            fetched: {...converted},
            current: {...converted}
          }
        })
      } else {
        const defaultPrefs: IPreferences = {
          directPhone: '',
          extension: '',
          mennoSimons: false,
          name: graph.userDisplayName,
          pronouns: "",
          title: graph.jobTitle,
          useScentFree: false,
          useSocialMedia: false,
          useTollFree: false,
          useTreatyStatement: false
        };

        this.setState({
          prefs: {
            kind: 'New',
            email: graph.email,
            current: defaultPrefs
          }
        })
      }
    }).catch((err) => {
      this.setState({
        prefs: {
          kind: 'Error',
          error: err.toString(),
          current: this.state.prefs.current
        }
      })
    });
  }

  public handleSave = (): void => {
    switch (this.state.prefs.kind) {
      case 'Error':
        return;

      case 'Fetching':
        return;

      case 'Fetched': {
        const id = this.state.prefs.id;
        const val = this.state.prefs.current;

        const updates: SPPreferences = {
          Direct_x0020_Phone: val.directPhone,
          Extension: val.extension,
          Job_x0020_Title: val.title,
          Menno_x0020_Simons: val.mennoSimons,
          Pronouns: val.pronouns,
          Title: val.name,
          UseSocialMedia: val.useSocialMedia,
          Use_x0020_Scent_x0020_Free: val.useScentFree,
          Use_x0020_Toll_x0020_Free: val.useTollFree,
          Use_x0020_Treaty_x0020_Statement: val.useTreatyStatement
        };

        this.props.sp.web.lists.getById(this.listGUID).items.getById(id).update(updates)
          .then(() => this.setState({
            prefs: {
              kind: 'Fetched',
              id: id,
              current: {...val},
              fetched: {...val}
            }
          })).catch(err => console.log(err));

        return;
      }

      case 'New': {
        const val = this.state.prefs.current;
        const updates: SPPreferences = {
          Direct_x0020_Phone: val.directPhone,
          Extension: val.extension,
          Job_x0020_Title: val.title,
          Menno_x0020_Simons: val.mennoSimons,
          Pronouns: val.pronouns,
          Title: val.name,
          UseSocialMedia: val.useSocialMedia,
          Use_x0020_Scent_x0020_Free: val.useScentFree,
          Use_x0020_Toll_x0020_Free: val.useTollFree,
          Use_x0020_Treaty_x0020_Statement: val.useTreatyStatement
        };

        this.props.sp.web.currentUser().then(u => {
          this.props.sp.web.lists.getById(this.listGUID).items.add({
            ...updates,
            PersonId: u.Id
          }).catch(err => console.log(err));
        }).catch(err => console.log(err));

        return;
      }
    }
  };

  public fetchMe (): Promise<IGraphData> {
    this.setState({graphState: {kind: "GraphFetching"}});
    
    return this.props.graph.me().then(u => {
      const data: IGraphData = {
        userDisplayName: u.displayName,
        jobTitle: u.jobTitle,
        businessPhones: u.businessPhones,
        mobilePhone: u.mobilePhone,
        email: u.userPrincipalName
      };

      this.setState({
        graphState: {
          kind: 'GraphData',
          data: data
        }});

      return data;
    }).catch((err) => {
      this.setState({
        graphState: {
          kind: 'GraphError'
        }
      });

      throw(err);
    });
  }

  public download = (event: React.MouseEvent<HTMLElement>): void => {
    event.preventDefault();

    const preview = this.renderPreview(this.state.prefs.current);
    const markup: string = ReactDOMServer.renderToStaticMarkup(preview);
    const blob = new Blob([markup], {type: 'text/html'});
    const fileDownloadUrl = URL.createObjectURL(blob);
    
    const link = document.createElement("a");
    link.href = fileDownloadUrl;
    link.setAttribute("download", "GeneratedSignature.htm");
    document.body.appendChild(link);
    link.click();

    URL.revokeObjectURL(fileDownloadUrl);
    link.parentNode.removeChild(link);
  }

  public handleDisplayName = (e: React.ChangeEvent<HTMLInputElement>): void => {
    this.setState((state, _) => {
      switch (state.prefs.kind) {
        case "New":
        case "Fetched":
          return {
            prefs: {
              ...state.prefs,
              current: {...state.prefs.current, name: e.target.value}
          }};
      }
    });
  }

  public validateUserDisplayName (prefs: IPreferences): string {
    if (!prefs.name) {
      return ("Your name is required.");
    } else {
      return null;
    }
  }

  public handlePronouns = (e: React.ChangeEvent<HTMLInputElement>): void => {
    this.setState((state, _) => {
      switch (state.prefs.kind) {
        case "New":
        case "Fetched":
          return {
            prefs: {
              ...state.prefs,
              current: {...state.prefs.current, pronouns: e.target.value}
          }};
      }
    });
  }

  public handleTitle = (e: React.ChangeEvent<HTMLInputElement>): void => {
    this.setState((state, _) => {
      switch (state.prefs.kind) {
        case "New":
        case "Fetched":
          return {
            prefs: {
              ...state.prefs,
              current: {...state.prefs.current, title: e.target.value}
          }};
      }
    });
  }

  public handleDirectPhone = (e: React.ChangeEvent<HTMLInputElement>): void => {
    this.setState((state, _) => {
      switch (state.prefs.kind) {
        case "New":
        case "Fetched":
          return {
            prefs: {
              ...state.prefs,
              current: {...state.prefs.current, directPhone: e.target.value}
          }};
      }
    });
  }

  public handleExtension = (e: React.ChangeEvent<HTMLInputElement>): void => {
    this.setState((state, _) => {
      switch (state.prefs.kind) {
        case "New":
        case "Fetched":
          return {
            prefs: {
              ...state.prefs,
              current: {...state.prefs.current, extension: e.target.value}
          }};
      }
    });

  }

  public handleTollFree = (event: React.MouseEvent<HTMLElement>, checked?: boolean): void => {
    this.setState((state, _) => {
      switch (state.prefs.kind) {
        case "New":
        case "Fetched":
          return {
            prefs: {
              ...state.prefs,
              current: {...state.prefs.current, useTollFree: checked}
          }};
      }
    });
  }

  public handleScentFree = (event: React.MouseEvent<HTMLElement>, checked?: boolean): void => {
    this.setState((state, _) => {
      switch (state.prefs.kind) {
        case "New":
        case "Fetched":
          return {
            prefs: {
              ...state.prefs,
              current: {...state.prefs.current, useScentFree: checked}
          }};
      }
    });
  }

  public handleUseTreatyStatement = (event: React.MouseEvent<HTMLElement>, checked?: boolean): void => {
    this.setState((state, _) => {
      switch (state.prefs.kind) {
        case "New":
        case "Fetched":
          return {
            prefs: {
              ...state.prefs,
              current: {...state.prefs.current, useTreatyStatement: checked}
          }};
      }
    });
  }

  public handleUseSocialMedia = (event: React.MouseEvent<HTMLElement>, checked?: boolean): void => {
    this.setState((state, _) => {
      switch (state.prefs.kind) {
        case "New":
        case "Fetched":
          return {
            prefs: {
              ...state.prefs,
              current: {...state.prefs.current, useSocialMedia: checked}
          }};
      }
    });
  }

  public handleMennoSimons = (event: React.MouseEvent<HTMLElement>, checked?: boolean): void => {
    this.setState((state, _) => {
      switch (state.prefs.kind) {
        case "New":
        case "Fetched":
          return {
            prefs: {
              ...state.prefs,
              current: {...state.prefs.current, mennoSimons: checked}
          }};
      }
    });
  }

  public renderForm (current: IPreferences, fetched: IPreferences, graph: IGraphData): React.ReactElement<ISignatureGeneratorProps> {
    return (
      <div className="ms-Grid">
        <div className="ms-Grid-row">
          <div className="ms-Grid-col ms-sm8">
            <TextField
              label="Name"
              value={current.name}
              onChange={this.handleDisplayName}
              errorMessage={this.validateUserDisplayName(current)} />
            { current.name === graph.userDisplayName ? null : <div>Default: {graph.userDisplayName}</div> }
          </div>

          <div className="ms-Grid-col ms-sm4">
            <TextField label="Pronouns" value={current.pronouns} onChange={this.handlePronouns} />
          </div>
        </div>

        <div className="ms-Grid-row">
          <div className="ms-Grid-col ms-sm12">
            <TextField multiline label="Title" value={current.title} onChange={this.handleTitle} />
            { current.title === graph.jobTitle ? null : <div>Default: {graph.jobTitle} </div> }
          </div>
        </div>

        <div className="ms-Grid-row">
          <div className="ms-Grid-col ms-sm8">
            <TextField label="Direct Phone" value={current.directPhone} onChange={this.handleDirectPhone} />
          </div>

          <div className="ms-Grid-col ms-sm4">
            <TextField label="Extension" value={current.extension} onChange={this.handleExtension} />
          </div>
        </div>

        <div className="ms-Grid-row" style={{marginTop: "12px"}}>
          <div className="ms-Grid-col ms-sm12">
            <Toggle label="Show toll-free number" inlineLabel checked={current.useTollFree} onChange={this.handleTollFree} />
            <Toggle label="Show scent free statement" inlineLabel checked={current.useScentFree} onChange={this.handleScentFree} />
            <Toggle label="Show treaty statement" inlineLabel checked={current.useTreatyStatement} onChange={this.handleUseTreatyStatement} />
            <Toggle label="Show social media icons" inlineLabel checked={current.useSocialMedia} onChange={this.handleUseSocialMedia} />
            <Toggle label="Use Menno Simons logo" inlineLabel checked={current.mennoSimons} onChange={this.handleMennoSimons} />
          </div>
        </div>

        <div className="ms-Grid-row">
          <div className="ms-Grid-col ms-sm12">
            <PrimaryButton text="Save" onClick={this.handleSave} />
          </div>
        </div>
      </div>
    )
  }

  public renderScent (prefs: IPreferences): React.ReactElement<ISignatureGeneratorProps> {
    if (prefs.useScentFree) {
      return (
        <p style={{fontStyle: "italic", margin: "4px 0px"}} dangerouslySetInnerHTML={escape(this.props.scentFree)} />
      );
    } else {
      return null;
    }
  }

  public renderTreatyStatement (prefs: IPreferences): React.ReactElement<ISignatureGeneratorProps> {
    if (prefs.useTreatyStatement) {
      return (
        <p style={{fontStyle: "italic", margin: "4px 0px"}}>
          <span dangerouslySetInnerHTML={escape(this.props.treatyStatement)} />
          <br />
          <span dangerouslySetInnerHTML={escape(this.props.metisStatement)} />
        </p>
      );
    } else {
      return null;
    }
  }

  public renderWebsiteUrl (prefs: IPreferences): React.ReactElement<ISignatureGeneratorProps> {
    const msUrl = prefs.mennoSimons
      ? (<span> | <a href={this.props.msWebsiteUrl} dangerouslySetInnerHTML={escape(this.props.msWebsiteText)} /></span>)
      : null;
          
    return (
      <span><a href={this.props.websiteUrl} dangerouslySetInnerHTML={escape(this.props.websiteText)} /> {msUrl}</span>
    );
  }

  public renderPhone (prefs: IPreferences): React.ReactElement<ISignatureGeneratorProps> {
    const directPhone = prefs.directPhone
      .replace(/[^0-9]/g, "")
      .replace(/^(...)(...)(....)/, "$1.$2.$3");

    return (
      <span><br />
        { prefs.useTollFree ? this.props.tollFreeNumber + " | " : "" }
        { directPhone }
        { (directPhone !== "" && prefs.extension !== "") ? " OR " : ""}
        { prefs.extension !== "" ? this.props.mainNumber + ", ext. " + prefs.extension : ""}
        { (directPhone !== "" || prefs.extension !== "") ? " | " : ""}

        { this.renderWebsiteUrl(prefs) }
      </span>
    );
  }

  public renderPronouns (prefs: IPreferences): React.ReactElement<ISignatureGeneratorProps> {
    const cleaned = prefs.pronouns
      .replace(/[^A-Z,a-z]+/g, "/") // replace runs of non-characters with /
      .replace(/^\//, "") // replace leading slash
      .replace(/\/$/, ""); // replace trailing slash

    if (cleaned) {
      return (
        <span> (cleaned))</span>
      );
    } else {
      return null;
    }
  }

  public renderName (prefs: IPreferences): React.ReactElement<ISignatureGeneratorProps> {
    return (
      <span style={{fontSize: "11pt"}}>
        <span style={{fontWeight: "bold"}} dangerouslySetInnerHTML={escape(prefs.name)} />
        { this.renderPronouns(prefs) }
      </span>
    );
  }

  public renderTitle (prefs: IPreferences): React.ReactElement<ISignatureGeneratorProps> {
    if (prefs.title) {
      const lines = prefs.title.split(/\r?\n/).map((line, index) => (
        <React.Fragment key={index}>
          <br />
          <span style={{fontStyle: "italic"}} dangerouslySetInnerHTML={escape(line)} />
        </React.Fragment>
      ));

      return (
        <React.Fragment>
          { lines }
        </React.Fragment>
      );
    } else {
      return null;
    }
  }

  public renderAddress (prefs: IPreferences): React.ReactElement<ISignatureGeneratorProps> {
    if (prefs.mennoSimons) {
      return (
        <React.Fragment>
          <br />
          <span dangerouslySetInnerHTML={escape(this.props.msAddress)} />
        </React.Fragment>
      );
    } else {
      return null;
    }
  }

  public renderLogo (prefs: IPreferences): React.ReactElement<ISignatureGeneratorProps> {
    if (prefs.mennoSimons) {
      return (
          <p style={{margin: "4px 0px"}}>
            <img src={this.props.imagePrefix + "/msc.emailsignature.png"} alt="Menno Simons College logo" />
          </p>
      );
    } else {
      return (
        <p style={{margin: "4px 0px"}}>
          <img src={this.props.imagePrefix + "/cmu.emailsignature.png"} alt="CMU Logo" />
        </p>
      );
    }
  }

  public renderSocialMedia (prefs: IPreferences): React.ReactElement<ISignatureGeneratorProps> {
    if (prefs.useSocialMedia) {
      return (
        <p style={{margin: "4px 0px"}}>
          <a href="https://www.facebook.com/CMUwpg">
            <img style={{verticalAlign: "middle"}} src={this.props.imagePrefix + "/fbook.emailsignature.png"} alt="Facebook" />
          </a>
          <a href="https://twitter.com/CMUwpg">
            <img style={{verticalAlign: "middle", paddingLeft: "4px"}} src={this.props.imagePrefix + "/twitter.emailsignature.png"} alt="Twitter" />
          </a>
          <a href="https://www.instagram.com/cmuwpg/">
            <img style={{verticalAlign: "middle", paddingLeft: "4px"}} src={this.props.imagePrefix + "/instagram.emailsignature.png"} alt="Instagram" />
          </a>
        </p>
      );
    } else {
      return null;
    }
  }

  public renderPreview (prefs: IPreferences): React.ReactElement<ISignatureGeneratorProps> {
    return (
      <div id="generated-signature" style={{fontFamily: '"Myriad Pro", sans-serif', fontSize: "10pt"}}>
        <p style={{margin: "4px 0px"}}>
          { this.renderName(prefs) }
          { this.renderTitle(prefs) }
          { this.renderPhone(prefs) }
          { this.renderAddress(prefs) }
        </p>
        { this.renderLogo(prefs) }
        { this.renderSocialMedia(prefs) }
        { this.renderScent(prefs) }
        { this.renderTreatyStatement(prefs) }
      </div>
    );
  }

  public render (): React.ReactElement<ISignatureGeneratorProps> {
    switch (this.state.graphState.kind) {
      case 'GraphFetching':
        return (
          <div>Fetching default data. <Spinner size={SpinnerSize.large} />
          </div>
        )

      case 'GraphError':
        return (
          <div>There was an error fetching default data.</div>
        )

      case 'GraphData':
        switch(this.state.prefs.kind) {
          case "Error":
            return (
              <div>
                There has been some kind of error fetching your preferences.
              </div>
            )

          case 'Fetching':
            return (
              <div>Fetching your preferences. <Spinner size={SpinnerSize.large} />
              </div>
            )

          case "New": {
            const preview = this.renderPreview(this.state.prefs.current);

            return (
              <section className={`${styles.signatureGenerator} ${this.props.hasTeamsContext ? styles.teams : ''}`}>
                <div className="ms-Grid">
                  <div className="ms-Grid-row">
                    <div className="ms-Grid-col ms-sm12 ms-lg8">
                      { this.renderForm(this.state.prefs.current, null, this.state.graphState.data) }
                    </div>
                    <div className="ms-Grid-col ms-sm12 ms-lg4">
                      { preview }
                      <div style={{marginTop: "24pt"}}>
                        <PrimaryButton text="Download Signature" onClick={this.download} />
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            );
          }

          case 'Fetched': {
            const preview = this.renderPreview(this.state.prefs.current);

            return (
              <section className={`${styles.signatureGenerator} ${this.props.hasTeamsContext ? styles.teams : ''}`}>
                <div className="ms-Grid">
                  <div className="ms-Grid-row">
                    <div className="ms-Grid-col ms-sm12 ms-lg8">
                      { this.renderForm(this.state.prefs.current, this.state.prefs.fetched, this.state.graphState.data) }
                    </div>
                    <div className="ms-Grid-col ms-sm12 ms-lg4">
                      { preview }
                      <div style={{marginTop: "24pt"}}>
                        <PrimaryButton text="Download Signature" onClick={this.download} />
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            );
          }
        }
    }
  }
}