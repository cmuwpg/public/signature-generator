define([], function() {
  return {
    "PropertyPaneDescription": "Formats an email signature according to CMU style",
    "BasicGroupName": "Settings",
    "TollFreeNumberFieldLabel": "Toll Free Number",
    "MainPhoneNumberFieldLabel": "Main Phone Number",
    "WebsiteTextFieldLabel": "Website Text",
    "ImagePrefixFieldLabel": "Image Prefix",
    "WebsiteUrlFieldLabel": "Website URL",
    "MsWebsiteTextFieldLabel": "Menno Simons Website Text",
    "MsWebsiteUrlFieldLabel": "Menno Simons Website URL",
    "MsAddress": "Menno Simons Address",
    "ScentFreeFieldLabel": "Scent Free Statement",
    "TreatyStatementFieldLabel": "Treaty Statement",
    "MetisStatementFieldLabel": "Metis Statement"
  }
});