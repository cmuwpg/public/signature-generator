declare interface ISignatureGeneratorWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  TollFreeNumberFieldLabel: string;
  MainPhoneNumberFieldLabel: string;
  ScentFreeFieldLabel: string;
  TreatyStatementFieldLabel: string;
  MetisStatementFieldLabel: string;
  WebsiteTextFieldLabel: string;
  ImagePrefixFieldLabel: string,
  WebsiteUrlFieldLabel: string;
  MsWebsiteTextFieldLabel: string;
  MsWebsiteUrlFieldLabel: string;
  MsAddressFieldLabel: string;
}

declare module 'SignatureGeneratorWebPartStrings' {
  const strings: ISignatureGeneratorWebPartStrings;
  export = strings;
}
